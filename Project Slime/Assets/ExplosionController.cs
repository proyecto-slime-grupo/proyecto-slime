﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionController : MonoBehaviour
{
    public GameObject particulas;
    public float duracion;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(TiempoVidaExplosion());
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Enemigo")
        {
            Debug.Log("Exploto " + other.name);
            Destroy(other.gameObject);
        }
    }

    IEnumerator TiempoVidaExplosion()
    {
        Instantiate(particulas,transform.position,transform.rotation);
        yield return new WaitForSeconds(duracion);
        Destroy(gameObject);
    }
}
