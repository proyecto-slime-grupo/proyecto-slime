﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcidoController : MonoBehaviour
{
    public float duracion;
    public GameObject acidoParticulas;
    public GameObject gasParticulas;


    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(TiempoVidaAcido());
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Enemigo")
        {
            Destroy(other.gameObject);
        }
    }

    IEnumerator TiempoVidaAcido()
    {
        Instantiate(acidoParticulas, transform.position, transform.rotation);
        Instantiate(gasParticulas, transform.position, transform.rotation);
        yield return new WaitForSeconds(duracion);
        Destroy(gameObject);
    }
}
