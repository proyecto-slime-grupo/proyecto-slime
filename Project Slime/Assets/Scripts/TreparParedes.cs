﻿using UnityEngine;

public class TreparParedes : MonoBehaviour
{
    Rigidbody2D rb;
    public float velocidad = 5f;
    public bool cercaPared = false;

    private Vector2 movimiento;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void TreparArriba()
    {
        float y = Input.GetAxis("Vertical");
        movimiento = new Vector2(0, y);
        rb.velocity = movimiento.normalized * velocidad; 
    }

    void TreparAbajo()
    {
        float y = Input.GetAxis("Vertical");
        movimiento = new Vector2(0, y);
        rb.velocity = movimiento.normalized * velocidad;
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if(other.tag=="ParedTrigger")
        {
            cercaPared = true;
           // rb.transform.localRotation = Vector3.z;

            GameObject pared = GameObject.FindWithTag("ParedAuxiliar");
            pared.GetComponent<Collider2D>().enabled = true;

            if (cercaPared && Input.GetKey(KeyCode.UpArrow))
            {
                TreparArriba();
            }

            if (cercaPared && Input.GetKey(KeyCode.DownArrow))
            {
                TreparAbajo();
            }
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        GameObject pared = GameObject.FindWithTag("ParedAuxiliar");
        pared.GetComponent<Collider2D>().enabled = false;
        if (other.tag == "ParedTrigger")
        {
            cercaPared = false;           
        }
    }
}
