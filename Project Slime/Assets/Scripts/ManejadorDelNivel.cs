﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManejadorDelNivel : MonoBehaviour
{
    public GameObject checkPointActual;
    private MoverCientifico cientifico;
    // Start is called before the first frame update
    void Start()
    {
        cientifico = FindObjectOfType<MoverCientifico>();
    }

    public void RespawnearJugador()
    {
        cientifico.transform.position = checkPointActual.transform.position;
    }
}
