﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class VidaCientifico : MonoBehaviour
{
    public int vida;
    public int cantidadVida;

    public Image  vidaUI;
    public Sprite vidaLlena;
    public Sprite vidaMedia;
    public Sprite vidaVacia;
    public Sprite vidaPerdida;

    public float tiempoInvencible = 3f;

    public Rigidbody2D rb;
    public float dirX = 5f;
    private bool viendoDerecha = true;

    public ManejadorDelNivel manejador;
    MoverCientifico controller;


    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        controller = GetComponent(typeof(MoverCientifico)) as MoverCientifico;
    }
    void Update()
    {
            switch (vida)
            {
                case 3:
                    vidaUI.sprite = vidaLlena;
                    break;
                case 2:
                    vidaUI.sprite = vidaMedia;
                    break;
                case 1:
                    vidaUI.sprite = vidaVacia;
                    break;
                case 0:
                    vidaUI.sprite = vidaPerdida;
                    MatarCientifico();                 
                    break;                  
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag=="Enemigo" && vida > 0 && !Input.GetKey(KeyCode.X))
        {
            vida -= 1;
            StartCoroutine("Doler");
        }
        else
        {
            dirX = 0;
        }
    }


    IEnumerator Doler()
    {       
        rb.velocity = Vector2.zero;

        if (viendoDerecha)
            rb.AddForce(new Vector2(-200f, 200f));
        else
            rb.AddForce(new Vector2(200f, 200f));

        yield return new WaitForSeconds(0.5f);
    }

    public void MatarCientifico()
    {
        manejador = FindObjectOfType<ManejadorDelNivel>();
        manejador.RespawnearJugador();
        vida = 3;
    }
}
