﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SistemaCrafteo : MonoBehaviour
{
    public List<CreacionDePociones> listaPociones;
    Inventario inventario;

    public Transform puntoDisparo;
    public List<GameObject> efectoATirar;

    // Start is called before the first frame update
    void Start()
    {
        inventario = GetComponent<Inventario>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.C))
        {
            string ingrediente1 = inventario.slots[0].name;
            string ingrediente2 = inventario.slots[1].name;

            MezclarPocion(ingrediente1, ingrediente2);
        }    
    }

    public void MezclarPocion(string ingre1,string ingre2)
    {
        for (int i = 0; i < listaPociones.Count; i++)
        {
            if (ingre1 == listaPociones[i].ingrediente1 && ingre2 == listaPociones[i].ingrediente2 ||
                ingre2 == listaPociones[i].ingrediente1 && ingre1 == listaPociones[i].ingrediente2)
            {
                Instantiate (efectoATirar[i], puntoDisparo.position, puntoDisparo.rotation);              
                LimpiarInventario();
            }
        }
        
    }

    public void LimpiarInventario()
    {
        int i = 0;
        foreach (var item in inventario.slots)
        {
            inventario.slots[i].name = "Slot(" + i + ")";
            Debug.Log("Se borro" + inventario.slots[i]);
            Destroy(inventario.slots[i].transform.GetChild(0).gameObject);
            i++;
        }       
    }
}
