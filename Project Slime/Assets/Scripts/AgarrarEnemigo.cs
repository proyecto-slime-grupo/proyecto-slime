﻿using UnityEngine;

public class AgarrarEnemigo : MonoBehaviour
{
    private Inventario inventario;
    public GameObject item;

    private void Start()
    {      
        inventario = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventario>();
        for (int i = 0; i < inventario.slots.Length; i++)
        {
                inventario.estaLleno[i] = true;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player") && Input.GetKey(KeyCode.X))
        {
            for (int i = 0; i < inventario.slots.Length; i++)
            {
                if (!inventario.estaLleno[i])
                {
                    inventario.estaLleno[i] = true;
                    Instantiate(item,inventario.slots[i].transform,false);
                    if(item.name=="acido-icon")
                    {
                        inventario.slots[i].name = "acido";
                    }
                    if (item.name == "SlimeElectricoCapturado")
                    {
                        inventario.slots[i].name = "rayo";
                    }
                    if (item.name == "Fuego-icon")
                    {
                        inventario.slots[i].name = "fuego";
                    }
                    Destroy(gameObject);
                    break;
                }
            }
        }
    }
}
        
