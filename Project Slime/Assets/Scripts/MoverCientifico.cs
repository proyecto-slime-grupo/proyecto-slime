﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoverCientifico : MonoBehaviour
{
    [Range(1, 10)]
    public float velocidadSalto;
    public float multiplicadorCaida=2.5f;
    public float multiplicadorSaltoCorto = 2f;
    Rigidbody2D rb;
    public float rapidezMovimiento=15f;
    public Transform checkearSuelo;
    public float radioSuelo;
    public LayerMask tierra;
    public bool estarEnSuelo;

    Vector3 direccionPersonaje;
    // Start is called before the first frame update
    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        direccionPersonaje = transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Input.GetAxis("Horizontal") * rapidezMovimiento * Time.deltaTime, 0f, 0f);
        if (Input.GetKeyDown(KeyCode.Space) && estarEnSuelo)
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.up * velocidadSalto;
        }

        if(Input.GetAxis("Horizontal")<0)
        {
            direccionPersonaje.x = -18;
        }

        if (Input.GetAxis("Horizontal") > 0)
        {
            direccionPersonaje.x = 18;
        }
        transform.localScale = direccionPersonaje;
    }

    void FixedUpdate()
    {
        if(rb.velocity.y<0)
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (multiplicadorCaida - 1) * Time.deltaTime;
        }
        else if(rb.velocity.y>0 && !Input.GetKey(KeyCode.Space))
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (multiplicadorSaltoCorto - 1) * Time.deltaTime;
        }

        estarEnSuelo = Physics2D.OverlapCircle(checkearSuelo.position, radioSuelo, tierra);

    }
}
