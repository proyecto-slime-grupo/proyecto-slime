﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BolaFuegoController : MonoBehaviour
{
    public float velocidad;

    private MoverCientifico mc;
    // Start is called before the first frame update
    void Start()
    {
        mc = FindObjectOfType<MoverCientifico>();
        if(mc.transform.localScale.x<0)
        {
            velocidad = -velocidad;
            transform.localScale = new Vector2(-3, 3);
        }
        else
        {
            transform.localScale = new Vector2(3, 3);
        }
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(velocidad, GetComponent<Rigidbody2D>().velocity.y);        
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag=="Enemigo")
        {
            Destroy(other.gameObject);
            Destroy(gameObject);
        }       
    }
}
