﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Invulnerabilidad : MonoBehaviour
{
    Renderer rend;
    Color c;
    VidaCientifico cientifico;

    void Start()
    {
        rend = GetComponent<Renderer>();
        c = rend.material.color;
        cientifico = GetComponent<VidaCientifico>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag=="Enemigo" && cientifico.vida > 0 && !Input.GetKey(KeyCode.X))
            StartCoroutine("VolverseInvulnerable");
    }

    IEnumerator VolverseInvulnerable()
    {
        Physics2D.IgnoreLayerCollision(9, 10, true);
        c.a = 0.5f;
        rend.material.color = c;
        yield return new WaitForSeconds(3f);
        Physics2D.IgnoreLayerCollision(9, 10, false);
        c.a = 1f;
        rend.material.color = c;
    }
}
