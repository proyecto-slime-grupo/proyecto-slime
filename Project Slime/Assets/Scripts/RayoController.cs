﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayoController : MonoBehaviour
{  
    public float duracion;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(TiempoVidaRayo());
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Enemigo")
        {
            Destroy(other.gameObject);
        }
    }

    IEnumerator TiempoVidaRayo()
    {
        yield return new WaitForSeconds(duracion);
        Destroy(gameObject);
    }
}
